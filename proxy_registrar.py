import socketserver
import sys
import json
import socket
from time import time, strftime, gmtime
from xml.dom import minidom
from uaclient import SmallSMILHandler, log


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    DicUsers = {}

    def json2register(self):
        try:
            with open(database, 'r') as jsonfile:
                self.DicUsers = json.load(jsonfile)
        except FileNotFoundError:
            print("Carpeta creada")

    def register2json(self):
        with open(database, "w") as fiche:
            json.dump(self.DicUsers, fiche, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        self.json2register()
        line = self.rfile.read()
        formato = line.decode('utf-8')
        log('Received from ' + self.client_address[0] + ':' +
            str(self.client_address[1]) + ': ' + formato, LOG)
        print(formato)
        METODO = formato.split(" ")[0].lower()

        if METODO == 'register':
            print('register')
            CORREO = formato.split(" ")[1].split(":")[1]
            PUERTO = int(formato.split(" ")[1].split(":")[2])
            IP_CLIENT = self.client_address[0]
            EXPIRA = int(formato.split('\r\n')[1].split(" ")[1])
            NOW = time()
            if EXPIRA != 0:
                self.DicUsers[CORREO] = {"PUERTO": PUERTO, "IP": IP_CLIENT,
                                         "EXPIRES": EXPIRA, "NOW": NOW}
                self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
                log('Sent to ' + self.client_address[0] + ':' +
                    str(self.client_address[1]) + ': SIP/2.0 200 OK', LOG)
            else:
                if CORREO in self.DicUsers:
                    del self.DicUsers[CORREO]
                    self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
                    log('Sent to ' + self.client_address[0] + ':' +
                        str(self.client_address[1]) + ': SIP/2.0 200 OK', LOG)
                else:
                    self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                    log('Sent to ' + self.client_address[0] + ':' +
                        str(self.client_address[1]) +
                        ': SIP/2.0 404 User Not Found', LOG)

        elif METODO == 'invite':
            print("invite")
            CORREO_DESTINO = \
                formato.split('\r\n')[0].split(' ')[1].split(':')[1]
            CORREO_ORIGEN = \
                formato.split('\r\n')[5].split(' ')[0].split('=')[1]
            print(CORREO_DESTINO, CORREO_ORIGEN)
            if CORREO_ORIGEN in self.DicUsers and \
                    CORREO_DESTINO in self.DicUsers:
                IP_DESTINO = self.DicUsers[CORREO_DESTINO]["IP"]
                PUERTO_DESTINO = self.DicUsers[CORREO_DESTINO]["PUERTO"]
                print(IP_DESTINO, PUERTO_DESTINO)
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IP_DESTINO, PUERTO_DESTINO))
                    my_socket.send(bytes(formato, "utf-8"))
                    log('Sent to ' + IP_DESTINO + ':' +
                        str(PUERTO_DESTINO) + ': ' + formato, LOG)
                    data = my_socket.recv(1024)
                    log('Received from ' + IP_DESTINO + ':' +
                        str(PUERTO_DESTINO) + ': ' + data.decode("utf-8"), LOG)
                    print(data.decode("utf-8"))
                self.wfile.write(data)
                log('Sent to ' + self.client_address[0] + ':' +
                    str(self.client_address[1]) +
                    ': ' + data.decode("utf-8"), LOG)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                log('Sent to ' + self.client_address[0] + ':' +
                    str(self.client_address[1]) +
                    ': SIP/2.0 404 User Not Found', LOG)

        elif METODO == 'bye':
            CORREO_DESTINO = \
                formato.split('\r\n')[0].split(' ')[1].split(':')[1]
            if CORREO_DESTINO in self.DicUsers:
                IP_DESTINO = self.DicUsers[CORREO_DESTINO]["IP"]
                PUERTO_DESTINO = self.DicUsers[CORREO_DESTINO]["PUERTO"]
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IP_DESTINO, PUERTO_DESTINO))
                    my_socket.send(bytes(formato, "utf-8"))
                    log('Sent to ' + IP_DESTINO + ':' +
                        str(PUERTO_DESTINO) + ': ' + formato, LOG)
                    data = my_socket.recv(1024)
                    print(data.decode("utf-8"))
                    log('Received from ' + IP_DESTINO + ':' +
                        str(PUERTO_DESTINO) + ': ' + data.decode("utf-8"), LOG)
                self.wfile.write(data)
                log('Sent to ' + self.client_address[0] + ':' +
                    str(self.client_address[1]) + ': ' +
                    data.decode("utf-8"), LOG)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                log('Sent to ' + self.client_address[0] + ':' + str(
                    self.client_address[1]) +
                    ': SIP/2.0 404 User Not Found', LOG)

        elif METODO == 'ack':
            CORREO_DESTINO = \
                formato.split('\r\n')[0].split(' ')[1].split(':')[1]
            if CORREO_DESTINO in self.DicUsers:
                IP_DESTINO = \
                    self.DicUsers[CORREO_DESTINO]["IP"]
                PUERTO_DESTINO = self.DicUsers[CORREO_DESTINO]["PUERTO"]
                with socket.socket(socket.AF_INET,
                                   socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IP_DESTINO, PUERTO_DESTINO))
                    my_socket.send(bytes(formato, "utf-8"))
                    log('Sent to ' + IP_DESTINO + ':' +
                        str(PUERTO_DESTINO) + ': ' + formato, LOG)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                log('Sent to ' + self.client_address[0] + ':' + str(
                    self.client_address[1]) +
                    ': SIP/2.0 404 User Not Found', LOG)

        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n\r\n')
            log('Sent to ' + self.client_address[0] + ':' +
                str(self.client_address[1]) +
                ': SIP/2.0 405 Method Not Allowed', LOG)
        self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    CONFIGURACION = sys.argv[1]
    lista = minidom.parse(CONFIGURACION)
    LOG = lista.getElementsByTagName("log")[0].firstChild.data
    database = \
        lista.getElementsByTagName('database')[0].attributes['path'].value
    server = lista.getElementsByTagName('server')
    ip_server = server[0].attributes['ip'].value
    puerto_server = int(server[0].attributes['puerto'].value)
    serv = \
        socketserver.UDPServer((ip_server, puerto_server), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        log('Starting...', LOG)
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        log('Finishing.', LOG)

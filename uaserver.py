import socketserver
import sys
import simplertp
import random
import os
from xml.dom import minidom
from xml.sax import make_parser
from uaclient import SmallSMILHandler, log


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    DicRTP = {}

    def handle(self):
        # Escribe dirección y puerto del cliente (de tupla client_address)
        mensaje = self.rfile.read().decode("utf-8")
        log('Received from ' + self.client_address[0] + ':' +
            str(self.client_address[1]) + ': ' + mensaje, LOG)
        print(mensaje)
        METODO = mensaje.split(" ")[0]
        if METODO == "INVITE":
            IP_RTP = mensaje.split("\r\n")[5].split(" ")[1]
            PUERTO_RTP = int(mensaje.split("\r\n")[8].split(" ")[1])
            self.DicRTP["IP"] = IP_RTP
            self.DicRTP["PUERTO"] = PUERTO_RTP
            parte2 = "v=0\r\no=" + USUARIO + " 127.0.0.1\r\n" \
                     "s=misesion\r\nt=0\r\nm=audio " + RTPAUDIO + " RTP"
            LINE = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 " \
                   "Ringing\r\n\r\nSIP/2.0 200 OK\r\nContent-Type: " \
                   "application/sdp\r\nContent-Length: " + \
                   str(len(parte2)) + "\r\n\r\n" + parte2 + "\r\n\r\n"
            self.wfile.write(bytes(LINE, "utf-8"))
            log('Send to ' + self.client_address[0] + ':' +
                str(self.client_address[1]) + ': ' + LINE, LOG)
        if METODO == "BYE":
            self.DicRTP = {}
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
            log('SEnd to ' + self.client_address[0] + ':' +
                str(self.client_address[1]) + ': SIP/2.0 200 OK', LOG)
        if METODO == "ACK":
            ALEAT = random.randint(1, 10)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0,
                                  ext_flag=0,
                                  marker=0,
                                  cc=ALEAT)
            csrc = []
            for i in range(20):
                csrc.append(random.randint(0, 15))
            RTP_header.setCSRC(csrc)
            audios = simplertp.RtpPayloadMp3(AUDIO)
            simplertp.send_rtp_packet(RTP_header, audios,
                                      self.DicRTP["IP"],
                                      self.DicRTP["PUERTO"])
            os.system("cvlc rtp://@" + self.DicRTP["IP"] + ":" +
                      str(self.DicRTP["PUERTO"]))


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos

    FILE = sys.argv[1]
    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open(FILE))
    lista = sHandler.get_tags()
    AUDIO = lista["audio"]
    print(lista)

    if lista['uaserver_ip'] == " ":
        IP = "127.0.0.1"
    else:
        IP = lista['uaserver_ip']
    print("IP: " + IP)
    PORT = lista['uaserver_puerto']
    print("puerto: " + PORT)
    USUARIO = lista['account_username']
    print("puerto: " + USUARIO)
    PASSWORD = lista['account_passwd']
    RTPAUDIO = lista['rtpaudio_puerto']
    print("puerto: " + RTPAUDIO)
    PROXYIP = lista['regproxy_ip']
    print("puerto: " + PROXYIP)
    PROXYPUERTO = lista['regproxy_puerto']
    print("puerto: " + PROXYPUERTO)
    lista = minidom.parse(FILE)
    LOG = lista.getElementsByTagName('log')
    LOG = LOG[0].firstChild.data
    audio = lista.getElementsByTagName('log')
    audio = audio[0].firstChild.data

    serv = socketserver.UDPServer((IP, int(PORT)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()

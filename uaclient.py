import socket
import sys
import random
import simplertp
import os
from xml.dom import minidom
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time

# Cliente UDP simple.

# Dirección IP del servidor.


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        # lista de atributos
        self.current_tag = []
        self.atributos = {
            "account": ["username", "passwd"],
            "uaserver": ["ip", "puerto"],
            "rtpaudio": ["puerto"],
            "regproxy": ["ip", "puerto"],
            "log": [" "],
            "audio": [" "]}
        self.lista = {}

    def startElement(self, nombre, attrs):
        if nombre in self.atributos:
            self.current_tag = nombre
            for names in self.atributos[nombre]:
                self.lista[nombre + "_" + names] = attrs.get(names, "")

    def endElement(self, name):
        """Funcion para ejecutar etiquetas al final."""
        self.current_tag = ''

    def characters(self, content):
        """contenido de la etiqueta"""
        if self.current_tag == "log" or self.current_tag == "audio":
            self.lista[self.current_tag] = content

    def get_tags(self):
        return self.lista


def seconds(horas, minutos, segundos):
    """calculamos los segundos."""
    h = horas * 3600
    m = minutos * 60
    return h + m + segundos


def log(mensaje, log_path):
    """Abre un fichero log para poder escribir en el."""
    fich = open(log_path, "a")
    now = time.gmtime(time.time())
    fich.write(time.strftime('%Y%m%d%H%M%S', now) + ' ')
    fich.write(mensaje.replace('\r\n', ' ') + "\n")
    fich.close()


if __name__ == "__main__":
    FILE = sys.argv[1]
    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open(FILE))
    lista = sHandler.get_tags()
    AUDIO = lista["audio"]

    if lista['uaserver_ip'] == " ":
        IP = "127.0.0.1"
    else:
        IP = lista['uaserver_ip']
    PORT = lista['uaserver_puerto']
    USUARIO = lista['account_username']
    PASSWORD = lista['account_passwd']
    RTPAUDIO = lista['rtpaudio_puerto']
    PROXYIP = lista['regproxy_ip']
    PROXYPUERTO = lista['regproxy_puerto']
    lista = minidom.parse(FILE)
    LOG = lista.getElementsByTagName('log')
    LOG = LOG[0].firstChild.data
    audio = lista.getElementsByTagName('log')
    audio = audio[0].firstChild.data

    try:
        METHOD = sys.argv[2]
        MENSAJE = sys.argv[3]
    except (IndexError, ValueError):
        sys.exit('Usage: python3 client.py method receiver@IP.SIPport')

# Contenido que vamos a enviar

    LINE = METHOD + ' sip:' + MENSAJE + '@' + ' SIP/2.0\r\n'

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((PROXYIP, int(PROXYPUERTO)))
        log("Starting...", LOG)

        if METHOD == 'INVITE':
            parte2 = "\r\n\r\nv=0\r\no=" + USUARIO + " 127.0.0.1\r\n" \
                     "s=misesion\r\nt=0\r\nm=audio " + RTPAUDIO + " RTP"
            LINE = METHOD + " sip:" + MENSAJE + ' SIP/2.0\r\nContent-' \
                'Type: application/sdp\r\n' \
                'Content-Length: ' + str(len(parte2)) + parte2
        elif METHOD == 'REGISTER':
            LINE = (METHOD + ' sip:' + USUARIO + ':' + PORT +
                    ' SIP/2.0\r\n' + 'Expires: ' + MENSAJE)
        elif METHOD == 'BYE':
            LINE = METHOD + ' sip:' + MENSAJE + ' SIP/2.0\r\n'
        print("Enviando: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        log('Sent to ' + PROXYIP + ':' + PROXYPUERTO + ': ' + LINE, LOG)
        data = my_socket.recv(1024)
        log('Received from ' + PROXYIP + ':' + PROXYPUERTO +
            ': ' + data.decode("utf-8"), LOG)
        print('Recibido -- ', data.decode('utf-8'))

        if METHOD == 'INVITE':
            datos = data.decode('utf-8')
            if datos.find('SIP/2.0 180 Ringing') != -1 and \
                    datos.find('SIP/2.0 100 Trying') != -1 and \
                    datos.find('SIP/2.0 200 OK') != -1:
                LINEA = "ACK sip:" + MENSAJE + ' SIP/2.0\r\n'
                print("Enviando: " + LINEA)
                my_socket.send(bytes(LINEA, 'utf-8'))
                log('Sent to ' + PROXYIP +
                    ':' + PROXYPUERTO + ': ' + LINEA, LOG)
                IP_RTP = datos.split("\r\n")[9].split(" ")[1]
                PORT_RTP = int(datos.split("\r\n")[12].split(" ")[1])
                ALEAT = random.randint(1, 10)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(pad_flag=0,
                                      ext_flag=0,
                                      marker=0,
                                      cc=ALEAT)
                csrc = []
                for i in range(20):
                    csrc.append(random.randint(0, 15))
                RTP_header.setCSRC(csrc)
                audios = simplertp.RtpPayloadMp3(AUDIO)
                simplertp.send_rtp_packet(RTP_header, audios, IP_RTP, PORT_RTP)
                os.system("cvlc rtp://@" + IP_RTP + ":" + str(PORT_RTP))
        print("Terminando socket...")
        print("Fin.")
        log("Finishing...", LOG)

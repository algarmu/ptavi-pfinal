ptavi-pfinal
============

Ficheros para realizar la práctica final de PTAVI.

Asignatura y Grado:
Protocolos para la Transmisión de Audio y Vídeo en Internet
Grado en Ingeniería de Sistemas Audiovisuales y Multimedia

Departamento, Escuela y Universidad:
Grupo de Sistemas y Comunicaciones
Escuela Técnica Superior de Ingenieros de Telecomuncación
Universidad Rey Juan Carlos


Comentario: Realicé mal el fork (lo hice en el que no era).
A los pocos días de entregar la práctica me di cuenta y lo
realicé correctamente, pero para conservar algunos commits,
intenté volver a hacerla paso por paso e ir implementándola.
Por esta razón los commits no están muy bien hechos.
Le comenté a Jesús esto en una tutoría y me dijo que lo
explicase aqui en el README.
